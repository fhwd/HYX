#!/bin/bash

# bin2hyx
# arg1 : input file
# arg2 : offset (optionnal, must be valid hexa characters)
#  output file on stdout
#
# how to turn a binary file into a compact HYX1 file.
#  the removed space and the duplicate dot help to shrink the file.
#  However od replaces several bytes by a single *
#  it's painful to do a sed script that fixes that :-(

[ -r "$1" ] && {
  # header
  echo ';;hyx1'
  # optional label
  [ -n "$2" ] && echo ':'$2
  # convert to hex bytes, no duplication removal
  od -v -w1 -t x1 -An $1  |sed '
$!N
/^\(.*\)\n\1$/ {
  x
  s/$/\n./
  x
  D
}
P
x
/^$/! {
  s/^\n//
  p
  s/.*//
}
x
D'|sed '
$!N;$!N;$!N;$!N;$!N;$!N;$!N;$!N;
$!N;$!N;$!N;$!N;$!N;$!N;$!N;  #  gather 16 lines and remove spaces
s/[ \n]//g'
  echo '@'
}

#  and it's very slow...
