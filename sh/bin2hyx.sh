#!/bin/bash

# bin2hyx
# how to turn a binary file into a HYX0 file : it's so simple !

# arg1 : input file
# arg2 : offset (optionnal, must be valid hexa characters)
#  output file on stdout

[ -r "$1" ] && {
  echo ';;hyx0'
  [ -n "$2" ] && echo ':'$2
  od -v -w16 -t x1 -An $1
}

