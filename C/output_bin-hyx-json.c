/* Fichier : HYX/C/output_bin-hyx-json.c
created by Yann Guidon / ygdes.com
jeu. juil. 10 03:03:01 CEST 2014
sam. sept.  6 17:13:14 CEST 2014
dim. sept.  7 22:37:42 CEST 2014 : support '.' abbreviation, HYX1 format
mar. 17 oct. 2023 03:08:06 : Y8, git


The reverse of hyx2bin, outputs binary data in the desired file format.
Select the format, call init_func then display_func for each block of bytes.
Don't forget finish_func() at the end. See output_block() at the end.

Only contiguous ranges are allowed so far but you may call
output_block() several times (for the formats that support it).

TODO: compute checksums ?
*/

// expects erreur() to be already defined (see PI_GPIO.c)

FILE *export_fout;

unsigned int current_address=0;
int first=1;

void empty_func() {
  // empty
}
void empty_func1() {
  // empty
}

//// init

void init_hyx() {
  fprintf(export_fout, ";;hyx1\n");
  if (current_address)
    fprintf(export_fout, ":%X\n", current_address);
}

void init_json() {
  fprintf(export_fout, "{\n  start=%d,\n  data=[", current_address);
  first=1;
}

//// display

void display_bin(unsigned int size, unsigned char *s) {
  if (fwrite(s, size, 1, export_fout) != 1)
    erreur("Failed to write to binary output file");
  current_address+=size;
}

void display_hyx(unsigned int size, unsigned char *s) {
  int last=-1;
  unsigned char c;
  while (size--) {
    c=*s++;
    if (c == last)
      fputc('.', export_fout);
    else
      fprintf(export_fout, " %02X", c);
    last=c;

    if (!(~current_address & 15))
      fprintf(export_fout, " ; %Xxh\n", current_address >> 4);
    current_address++;
  }
}

void display_hyx_compact(unsigned int size, unsigned char *s) {
  int last=-1;
  unsigned char c;
  while (size--) {
    c=*s++;
    if (c == last)
      fputc('.', export_fout);
    else
      fprintf(export_fout, "%02X", c);
    last=c;
    current_address++;
  }
}

void display_json(unsigned int size, unsigned char *s) {
  while (size--) {
    if (first)
      first=0;
    else
      fputc(',',export_fout);

    if (!(current_address & 15))
      fputc('\n',export_fout);
    fprintf(export_fout, "%u", *(s++));
    current_address++;
  }
}

///// Comments
void comment_hyx(char *s) {
  fprintf(export_fout, " ; %s\n", s);
}

void comment_json(char *s) {
  fprintf(export_fout, " // %s\n", s);
}

//// finish

void finish_json() {
  fputs(" ]\n}\n", export_fout);
}

void finish_hyx() {
  fputs("\n@\n", export_fout);
}


//// function pointers

void (*init_func)() = init_json;
void (*display_func)(unsigned int size, unsigned char *s) = display_json;
void (*comment_func)(char *s) = comment_json;
void (*finish_func)() = finish_json;


void select_bin(){
  init_func = empty_func;
  display_func = display_bin;
  comment_func = empty_func1;
  finish_func = empty_func;
}

void select_hyx_compact(){
  init_func = init_hyx;
  display_func = display_hyx_compact;
  comment_func = empty_func1;
  finish_func = empty_func;
}

void select_hyx(){
  init_func = init_hyx;
  display_func = display_hyx;
  comment_func = comment_hyx;
  finish_func = finish_hyx;
}

void select_json(){
  init_func = init_json;
  display_func = display_json;
  comment_func = comment_json;
  finish_func = finish_json;
}

////////////////////////////////////////////

void output_block(unsigned char *buff, unsigned int size, unsigned int offset, FILE *xfile){
  export_fout=xfile;
  current_address=offset;
  init_func();
  display_func(size, buff); // May be called as many times as necessary
  finish_func();
}
