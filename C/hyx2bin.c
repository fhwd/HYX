/*
HYX/C/hyx2bin.c
  This file is distributed under terms of the Affero GPL v3 license or later, see http://yasep.org
created mer. févr. 15 18:59:04 CET 2012 by whygee@f-cpu.org
version jeu. févr. 16 09:39:53 CET 2012
version mer. juil.  9 23:56:41 CEST 2014 : renamed yhx2bin.c to hyx2bin.c
version ven. sept.  5 06:16:43 CEST 2014 : moved reusable chunks to input_functions_file.c and input_hyx.c
sam. sept.  6 20:34:31 CEST 2014 : verbose added
mar. 17 oct. 2023 02:16:33 CEST : YGREC8 revision

Example program that imports a .hyx (Yasep HeX) file
and transforms it to raw binary.

test command :
gcc -Wall -Os hyx2bin.c -o hyx2bin &&
./hyx2bin example.hyx example.bin &&
diff example.bin example.ref.bin

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Declare some functions required by input_hyx.c
void error(char *msg) {
  fputs(msg, stderr);
  exit(EXIT_FAILURE);
}

void error_no(char *msg) {
  perror(msg);
  exit(EXIT_FAILURE);
}

#include "input_hyx.c"

int verbose=0;
size_t offset=0, nb_bytes_out=0;
FILE *f_in, *f_out;

// Custom callbacks :

void my_put(char c) {
  if (verbose)
    fprintf(stderr,"%02X ",c&255);
  nb_bytes_out++;
  fputc(c, f_out);
}

void my_seek(unsigned int a) {
  if (a > (1ULL << 24))
    error("Address is too large");
  if (offset==0) {
    offset=a; // only done the first time
    fprintf(stderr,"\njumping to address %Xh\n",a);
  }
  if (a!=(offset + nb_bytes_out))
    error("Discontinuous address specifiers not supported for the binary format\n");
}

int my_get()  {
  return fgetc(f_in);
}

///////////////////////////////

int main(int argc, char **argv) {
  if ((argc != 3) && (argc != 4))
    error("\nUsage : hyx2bin (filename in) (filename out) [verbose]\nYou may use '-' to specify stdin and/or stdout.\n");

  // input file
  f_in=stdin;
  if (argv[1][0]!='-' || argv[1][1]!=0) {
    f_in=fopen(argv[1],"rb");
    if(f_in==NULL)
      error_no("Can't open input file for reading.");
  }
  else
    if (verbose)
      fprintf(stderr,"Reading HYX from stdin\n");

  // output file
  f_out=stdout;
  if (argv[2][0]!='-' || argv[2][1]!=0) {
    f_out=fopen(argv[2],"wb");
    if(f_out==NULL)
      error_no("Can't open output file for writing.");
  }
  else
    if (verbose)
      fprintf(stderr,"Writing binary to stdout\n");

  if ((argc == 4) && (memcmp(argv[3],"verbose",8)==0))
    verbose=1;

  read_hyx(my_get, my_put, my_seek, error);

  if (fflush(f_out) != 0)
    error_no("can't flush output file");
  fclose(f_out);
  if (verbose)
    fprintf(stderr,"\n%lu bytes written.\n",nb_bytes_out);

  return EXIT_SUCCESS;
}
