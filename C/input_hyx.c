/* HYX/C/input_hyx.c
  This file is distributed under terms of the Affero GPL v3 license or later, see http://yasep.org
created mer. févr. 15 18:59:04 CET 2012 by whygee@f-cpu.org
version jeu. févr. 16 09:39:53 CET 2012
version mer. juil.  9 23:56:41 CEST 2014 : renamed yhx2bin.c to hyx2bin.c
version ven. sept.  5 05:57:55 CEST 2014 : split from hyx2bin.c
version dim. sept.  7 00:21:43 CEST 2014 : parser structure redesigned/shortened/simplified

Provides a parser to import a .hyx (Yasep HeX) file.

see hyx2bin.c for testing and usage.

Syntax of a HYX file :
HYX0:
 * first bytes : signature + new line
 * comments start with ";" up to the end of the line.
 * data are single bytes in sequential order,
    byte at address 0 comes first.
    They are represented by an atomic pair of hexa digits.
 * Spaces (non digit characters like \t, \n, whatever)
    can come anywhere in the stream,
    as long as it's not in the middle of a byte or number.
 * Address specifiers consist of a ":" followed by
    as many hexa digits for the address, followed by a separator.
    the address is not checked in this parser because it is considered
    to be a byte stream, but the feature is reserved for later.
HYX1 :
 * the dot '.' repeats the last byte (handy to save space for FFFFFFF....)
 * '@' terminates parsing.

TODO : a CRC would be nice...

The methods are provided to the main routine with function pointers,
you have to provide your own, for example if you handle files or memory arrays.
The syntax is so simple that it is read linearly, not more than one
character at a time. No "unget" is needed.
*/

char HYX_signature[5]=";;hyx";

void read_hyx(int (*get)(), void (*put)(char), void (*seek)(unsigned int), void (*error)(char *msg)) {
  int a, c, last=-1, i,j=0, digit=0, type=0;
    // type=0 => byte mode
    // type=1 => address mode
    // type=2 => comment mode

  void hex_digit(int c){
    digit++;
    if (type==0) {  // data byte mode
      // accumulate the hex nibble
      j=(j<<4) | c;
      if(digit==2) {
        last=j;
        put(j);
        j=0;
        digit=0;
      }
    }
    else { // address mode
      if (digit>8)
        error("HYX: Too many digits in address");
      // accumulate the hex nibble
      a=(a<<4) | c;
    }
  }

  // check the header
  for (i=0; i<sizeof(HYX_signature); i++) {
    if (get() != HYX_signature[i])
      error("HYX header : Signature mismatch");
  }
  // HYX versions supported : 0 and 1 ('.')
  c=get();
  if (c!='0' && c!='1')
    error("HYX header : Signature mismatch (version not supported)");
  // Line feed
  c=get();
  if (c!=0xD && c!=0xA)
    error("HYX header : Signature mismatch (line feed)");

  // OK this header is valid. proceed to parse the bytes.
  digit=0;

  do {
    c=get();

    // comment
    if (type==2) {
      if ((c>=0) && (c!=0xD) && (c!=0xA))
        continue;
      else
        type=0; // exit from comment mode if EOF or end of line
    }

    // digit !
    if ( ((c>='0') && (c<='9')) ) hex_digit(c-'0');    else {
    if ( ((c>='A') && (c<='F')) ) hex_digit(c-'A'+10); else {
    if ( ((c>='a') && (c<='f')) ) hex_digit(c-'a'+10); else {

      // end of number => switch states
      if (type==0) {
        if (digit!=0)
          error("HYX data : unterminated byte (missing one hex. digit)");
      }
      else
        if (type==1) { // type=1 : end of address
          seek(a);
          digit=0;
          type=0;
        }

      if (c<0) // EOF
        break;

      // Handle the other cases and changes of modes :
      switch(c){
        // . : repeat the last byte
        case '.' :
          if (last == -1)
            error("HYX syntax error: can't repeat byte at start of stream");
          put(last);
          break;

        // Todo :
        // *X : repeat the last byte X times
        case '*' : error("HYX syntax error: '*' not supported");

        case ':' : // address specifier
          a=0;
          type=1;
          break;

       // Comment ?
      case ';' :
        type=2;
        // fold

// NOP:
        case 0x0A:
        case 0x0D:
        case 0x20:
        case '\t':
        case '@':
          break;

        default :
          error("HYX syntax error: unexpected character in stream");
      } // switch(c)

    }
    } // if (hexa)
    }

  } while (c != '@');
}
