/*
HYX/C/hyx2bin.c
  This file is distributed under terms of the Affero GPL v3 license or later, see http://yasep.org
created mar. 17 oct. 2023 02:39:34 CEST whygee@f-cpu.org

This example program converts a binary/raw file
into a .hyx (default), a .json or even a raw file
(i know, it's pointless but it's an example).

test command :
gcc -Wall -Os bin2hyx.c -o bin2hyx

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFSIZE (1UL<<12)
unsigned char bytebuffer[BUFFSIZE];


// Declare some functions required by output_bin-hyx-json.c
void erreur(char *msg) {
  fputs(msg, stderr);
  exit(EXIT_FAILURE);
}

void error_no(char *msg) {
  perror(msg);
  exit(EXIT_FAILURE);
}

#include "output_bin-hyx-json.c"

unsigned long int nb_bytes_out=0; // Could also be set by a command line argument
FILE *f_in, *f_out;

///////////////////////////////

int main(int argc, char **argv) {
  size_t block_size;

  if ((argc != 3) && (argc != 4))
    erreur("\nUsage : hyx2bin (filename in) (filename out) [hyx|hyxc|json|bin]\n"
          "You may use '-' to specify stdin and/or stdout.\n");

  // input file
  f_in=stdin;
  if (argv[1][0]!='-' || argv[1][1]!=0) {
    f_in=fopen(argv[1],"rb");
    if(f_in==NULL)
      error_no("Can't open input file for reading.");
  }
/*  else
    if (verbose)
      fprintf(stderr,"Reading HYX from stdin\n");*/

  // output file
  f_out=stdout;
  if (argv[2][0]!='-' || argv[2][1]!=0) {
    f_out=fopen(argv[2],"wb");
    if(f_out==NULL)
      error_no("Can't open output file for writing.");
  }
/*  else
    if (verbose)
      fprintf(stderr,"Writing binary to stdout\n"); */

  if (argc == 4) {
    if (memcmp(argv[3],"hyx",4)==0)
      select_hyx();
    else {
      if (memcmp(argv[3],"hyxc",5)==0)
        select_hyx_compact();
      else {
        if (memcmp(argv[3],"json",5)==0)
          select_json();
        else {
          if (memcmp(argv[3],"bin",4)==0)
            select_bin();
          else
            erreur("Sorry : unknown format\n");
        }
      }
    }
  } else
    select_hyx();

// The main loop : process chunks.
  export_fout=f_out; // Globals are baaaaad.
  init_func();
  while(1) {
    block_size = fread(bytebuffer, 1, BUFFSIZE, f_in);
    if (block_size < 1) {
        // feof() ?
      break;
    }
    display_func(block_size, bytebuffer);
    nb_bytes_out += block_size; // No overflow check, I know...
  }
  finish_func();

  if (fflush(f_out) != 0)
    error_no("can't flush output file");
  fclose(f_out);
//  fprintf(stderr,"\n%lu bytes written.\n",nb_bytes_out);

  return EXIT_SUCCESS;
}
