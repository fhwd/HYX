gcc -Wall -Os hyx2bin.c -o hyx2bin &&
gcc -Wall -Os bin2hyx.c -o bin2hyx &&
 ./hyx2bin ../examples/example.hyx example.bin verbose &&
 diff example.bin ../examples/example.ref.bin | echo ' Test 1 : OK  !'

./hyx2bin ../examples/example.hyx example.bin verbose &&
diff example.bin ../examples/example.ref.bin &&
echo 'Test 2 : ok !' || echo 'Test 2 : failure'
echo

dd if=/dev/urandom of=rnd.bin bs=1024 count=128 status=noxfer &&

echo ';;hyx0' > rnd.hyx &&
od -v -w16 -t x1 -An rnd.bin >> rnd.hyx &&
./hyx2bin rnd.hyx rnd.2.bin &&
diff rnd.2.bin rnd.bin &&
echo 'test 3 : ok !' || echo 'test 3 : failure'

./bin2hyx rnd.bin rnd.hyx hyxc &&
./hyx2bin rnd.hyx rnd.2.bin &&
diff rnd.2.bin rnd.bin &&
echo 'test 4 : ok !' || echo 'test 4 : failure'

echo "### Trying the must-fail tests : ###"
for i in ../examples/must_fail/*hyx; do 
  echo -n $i " : "
  ./hyx2bin $i - > /dev/null || echo ' ---> OK'
done
echo -n "### Trying the must-pass tests : ###"
./hyx2bin  ../examples/must_work/00_contiguous.hyx 00_contiguous.bin && 
  diff 00_contiguous.bin ../examples/must_work/00_contiguous.ref.bin &&
  echo '../examples/must_work/00_contiguous.hyx     ---> OK'

rm -f example.bin rnd.hyx rnd.bin rnd.2.bin 00_contiguous.bin
