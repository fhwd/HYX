-- file YGREC8/MISC/hyx_filters.vhdl
-- created lun. nov.  8 23:05:29 CET 2021 by Yann Guidon / ygdes.com
-- version ven. nov. 12 06:43:15 CET 2021
--
-- Export a .HYX formatted stream

library std;
    use std.textio.all;

package hyx is

  -- beware of the off-by-one in the index !
  Constant HexTable: string := "0123456789ABCDEF";
  function int2hexstr(
      val: integer;
      max: integer := 16;  -- maximum digits
      min: integer := 1    -- minimum digits to convert (0-ext)
  ) return string;
  -- this conversion function should be moved to a proper common file

  procedure WriteHYX(
    filename: string;       -- name of the file to write
    offset: natural;        -- eventual starting address
    size: natural;          -- number of bytes to write
    data: string;           -- points to array of "char"/bytes/uint8_t
    compact: integer := 0;  -- optional flag
    line_width: integer := 40);   -- how many char per line (max)

end hyx;

package body hyx is

  function int2hexstr(
      val: integer;
      max: integer := 16;  -- maximum digits
      min: integer := 1    -- minimum digits to convert (0-ext)
  ) return string is
    variable s : string(1 to max);
    variable v, i : integer;
  begin
    assert val >= 0
       report "value must not be negative"
       severity failure;
    v := val;
    i := max;
    loop
      s(i) := HexTable((v mod 16)+1);
      v := v / 16;
      i := i - 1;
      exit when (i=0) or (v=0 and ((i+min)<=max));
    end loop;

    return s(i to max);
  end int2hexstr;

  procedure WriteHYX(
   filename: string;
   offset: natural;
   size: natural;
   data: string;
   compact: integer := 0;
   line_width: integer := 40)
  is
    variable s : string(1 to line_width);
    variable i, last, n, p, o, d : natural;
    file xf : text open write_mode is filename;

  begin
    write (xf, ";;hyx1" & LF);

    -- Initial offset:
    if offset > 0 then
      write (xf, ':' & int2hexstr(offset, 16) & LF);
    end if;

    p := s'low;
    last := natural'high;
    for i in data'range loop
      d := character'pos(data(i));

      if d = last then
        s(p) := '.';
        p := p + 1;
      else
        s(p+1) := HexTable((d mod 16)+1);
        s(p)   := HexTable((d  /  16)+1);

        if compact = 0 then
          s(p+2) := ' ';
          p := p + 3;
        else
          p := p + 2;
        end if;
      end if;

      if line_width < (p+4) then
        s(p) := LF;
        write (xf, s(s'low to p));
        p := s'low;
      end if;
    end loop;

    if p /= s'low then
      s(p) := LF;
      write (xf, s(s'low to p));
    end if;

  end WriteHYX;

end hyx;
