#!/bin/bash

if ghdl -v |grep "mcode" ; then
  echo "This script does not support mcode, use GHDL with GCC or LLVM backend."
else
  ghdl -a hyx_filters.vhdl hyx_tb.vhdl &&
  ghdl -e hyx_tb &&
  ./hyx_tb &&
  cat hyx_tb?.hyx
fi
