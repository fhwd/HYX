-- file YGREC8/MISC/hyx_tb.vhdl
-- created mer. nov. 10 07:29:14 CET 2021 by Yann Guidon / whygee@f-cpu.org
--
-- test hyx_filters.vhdl
--
-- ghdl -a hyx_filters.vhdl hyx_tb.vhdl &&
-- ghdl -e hyx_tb &&
-- ./hyx_tb &&
-- cat hyx_tb*.hyx

Library work;
    use work.all;
    use work.hyx.all;

entity hyx_tb is
end hyx_tb;

architecture tb of hyx_tb is
begin

  bench: process
    variable s : string(5 to 300);
    variable i : integer;

  begin
    i := 1;
    for i in s'range loop
      s(i) := character'val(i mod 256);
    end loop;

    WriteHYX("hyx_tb1.hyx", 123, s'length, s, 0, 70);
    WriteHYX("hyx_tb2.hyx", 0, s'length, s, 1, 30);

    wait;
  end process;

end tb;


