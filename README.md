README.md
Created sam. 14 oct. 2023 23:22:05 CEST by whygee@f-cpu.org

# HYX : Hexadecimal encoding for binary dumps

See https://hackaday.io/project/17992-hyx-file-format for logs/design history

Originally developed for the YASEP and the SPI Flasher https://hackaday.io/project/7758-spi-flasher ,
it is now used for the development of the YGREC8 (and more to come).

The .HYX format evolved from using commands such as "od -v -w16 -t x1 -An",
the format is so simple that a little file covers the syntax and use, see examples/example.hyx

* ;;hyx0 is simply the dump of the above command after adding a header. (see sh/bin2hyx.sh )

* ;;hyx1 adds a dot to "repeat the last byte" (see sh/bin2hyx1.sh )

However .HYX is not just a fancy name over a trivial code, it supports
addresses and multiple consecutive spans to support moderately complex systems.
The directories examples/must_fail and examples/must_work contain test data to check
an implementation with some corner cases.

Unlike Intel or Motorola HEX files, there is no checksum for each line.
No checksum at all, despite it being easy to implement now (see
https://hackaday.io/project/178998-peac-pisano-with-end-around-carry-algorithm )

Source code license : Affero GPL v3 or later https://www.gnu.org/licenses/agpl-3.0.html

Documentation license : Creative Commons Attribution-NonCommercial-ShareAlike
https://creativecommons.org/licenses/by-nc-sa/4.0
